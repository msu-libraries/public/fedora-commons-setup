FEDORA COMMONS SETUP
====================================

Table of contents
------------------------------------
* [System Requirements](#system-requirements)
* [Java](#java)
* [Tomcat](#tomcat)
* [MySQL](#mysql)
* [Fedora Commons](#fedora-commons)
* [Rebuilding Content](#rebuilding-content)

System Requirementa
------------------------------------
This documentation is for Fedora Commons version 3.8.1 specifically and 
have not been tested on other versions.  

**Versions**  
* Ubuntu 16.04
* Fedora 3.8.`
* Tomcat 7.0.52
* Java JDK 8

Java
------------------------------------
Fedora runs on Tomcat which is a Java application, so installation of Java is required.
For this documentation, we'll be using Java 8 (OpenJDK). 

```
 apt-get install default-jdk
```

Set the JAVA_HOME environment variable, which is required because we are using a custom version of Java.

```
echo "JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64"  >>  /etc/environment
```

Tomcat
------------------------------------
Fedora comes bundled with a version of Tomcat, but allows for using a local install instead. We will be using
`tomcat7` from the Ubuntu repository. This version of Tomcat is approved by the official documentation. The reason why
we prefer to use a local install is that it will receive patches through applying normal updates to the OS.

To install on Ubuntu (and optionally the tomcat admin module):
```
aptitude install tomcat7 tomcat7-admin
```

To enable access to the tomcat-admin interface at `/manager`, add the following lines inside
the `/etc/tomcat7/tomcat-users.xml` file (within the 'tomcat-users' tag).
```
<role rolename="manager-gui"/>
<user username="my_login_name" password="secret_passwd" roles="manager-gui"/>
```

The default memory assigned to tomcat is usually very small and should be changed. To do this, first remove the
default setting. On Ubuntu this is found in `/etc/default/tomcat7`. The default here is 128MB; remove the
`-Xmx128M` from the `JAVA_OPTS` line. 

In that same file (`/etc/default/tomcat7`) uncomment the JAVA_HOME line and update the path to:

```
JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
```

For tomcat environment variables or flags, a common way to set them is by creating a `setenv.sh` file in tomcat's
bin directory, found at `/usr/share/tomcat7/bin/` for Ubuntu. Create the file and set the execute flag.
```
touch /usr/share/tomcat7/bin/setenv.sh
chmod +x /usr/share/tomcat7/bin/setenv.sh
```

The reason why the `setenv.sh` file is useful is because it is not part of the standard installation files and
will not be overwritten by a patch or upgrade. Inside the file, we will specify our new memory limits. Make sure these
numbers are below the maximum amount available. `Xms` is for starting memory usage and `Xmx` is for maximum memory
usage. In this example, we are setting a start of 2 gigabytes of RAM with a maximum of 6 gigabytes.  
```
#!/bin/sh

JAVA_OPTS="-Xms2048m -Xmx6144m"
export JAVA_OPTS
```

**Important Note**  
If you end up using more memory than your machine has, you will likely have various things not work, and may not receive log messages
or errors regarding the failure. In addition to Tomcat, a number of other applications will be requiring a fair amount of memory.
Be sure to leave enough (a minimum of 3 to 4 gigabyes) available for the remainder of the applications.  

Restart tomcat to have it load your changes:
```
systemctl restart tomcat7
```

Further, you need to make sure the tomcat port (default: 8080) is open on the firewall for access
in the desired IPs that will need admin access.  
**Note:** _Do NOT open this port to the world!_

Only open the firewall to **SPECIFIC IPs** that need access to tomcat services. Any IP not blocked by
the firewall, will have full admin privileges.  
**ONLY ALLOW SINGLE SPECIFIC IPs ACCESS TO PORT 8080 IF THEY REQUIRE IT!!**

For details on how to manage the firewall on an Ubuntu machine, read documentation or the man page on Uncomplicated Firewall (ufw).
```
man ufw
```

You should be able to verify that tomcat is running by going visiting the default "It works!" page
or logging into the manger page in your browser:
```
http://myserver:8080/
http://myserver:8080/manager/
```

MySQL
------------------------------------
Fedora makes use a database backend; we'll be using a MySQL compatible database for this documentation. For
assistance with this, contact a database admin.

Create a database and grant ALL access to a login from the server's IP address. The ALL access is required
because fedora creates its own schema. Connect to the server where the database will reside and create
your database. For example:
```
CREATE DATABASE myfedcomdb DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_bin;
GRANT ALL ON myfedcomdb.* to 'myserverlogin'@'127.0.0.1' IDENTIFIED BY 'a_unique_passwd';
FLUSH PRIVILEGES;
```

Fedora Commons
------------------------------------
Fedora Commons is repository service. If is designed to store both data and metadata with means of managing
both. It provides APIs for interacting with the repositories and can be interfaced with by other software.

Download the Fedora Commons software from the official site: https://wiki.duraspace.org/display/FEDORA38/Downloads

Fedora Commons uses Apache Maven2 for its development and build environment. To install on an Ubuntu machine:
```
wget https://archive.apache.org/dist/maven/maven-2/2.2.1/binaries/apache-maven-2.2.1-bin.tar.gz
tar xzvf apache-maven-2.2.1-bin.tar.gz
mv apache-maven-2.2.1 /usr/lib/
export PATH=/usr/lib/apache-maven-2.2.1/bin/:$PATH
```

Use `mvn -v` to confirm it is installed.  

To use an operating system maintained MySQL JDBC database library, it also must be installed. On Ubuntu this is:
```
aptitude install libmysql-java
```

Before installation, you should create the fedora install directory, typically `/usr/local/fedora/` and give
ownership of it to the service which will be running fedora, in this case `tomcat7`.
```
mkdir /usr/local/fedora/
chown tomcat7:tomcat7 /usr/local/fedora/
```

We should also set this as our `FEDORA_HOME` for tomcat. Open the `/usr/share/tomcat7/bin/setenv.sh` file again
and set the home there. After editing, the file might look like this:
```
#!/bin/sh

JAVA_OPTS="-Xms2048m -Xmx3072m"
export JAVA_OPTS

FEDORA_HOME="/usr/local/fedora"
export FEDORA_HOME
```

When installing fedora, a number of questions will be asked. If you have a `install.properties` file, it will
populate the answers from the contents of the file. This file should be passed to the install command.

Also, you can prevent permissions issues from cropping up by running the install as the user
that will be serving it; again this is `tomcat7`. Depending on whether you have a `install.properties` file, you may
run the install jar file with or without passing the file to it, as seen below.
```
wget http://sourceforge.net/projects/fedora-commons/files/fedora/3.8.1/fcrepo-installer-3.8.1.jar/download  
mv download fcrepo-installer-3.8.1.jar  

sudo -H -u tomcat7 java -jar fcrepo-installer-3.8.1.jar ./install.properties
```

When installing, you will first be prompted for the type of install to perform. 'quick' will result in an install where
all services come bundled from Fedora Commons. This is not good choice as they will not receive security patches. Choose
'custom' so we can specify our local tomcat service and dedicated MySQL server. Note that you will be asked to specify
where your local tomcat install is, where your local MySQL java driver is, and the connection parameters to the MySQL database.

For our example installation, the tomcat location is: `/var/lib/tomcat7`
For our example installation, the MySQL driver is: `/usr/share/java/mysql.jar`
The database authentication info will be whatever you specified in Part 2.

Here is an example `install.properties` file. If you are installing a newer version of Fedora, it is not recommended that
you re-use an `install.properties` file from a previous Fedora version. Note that it has escaped characters so all `\` characters should be removed if doing the manual install.  
```
#Install Options
#Wed Jan 06 14:02:46 EST 2016
ri.enabled=true
apia.auth.required=false
messaging.enabled=true
database.jdbcDriverClass=com.mysql.jdbc.Driver
upstream.auth.enabled=false
ssl.available=false
database.jdbcURL=jdbc\:mysql\://myexample/fedoradb?useUnicode\=true&amp;characterEncoding\=UTF-8&amp;autoReconnect\=true
messaging.uri=vm\:(broker\:(tcp\://localhost\:61616))
database.password=MYPASSWORD
database.mysql.driver=/usr/share/java/mysql.jar
database.username=fedorausr
fesl.authz.enabled=false
tomcat.shutdown.port=8005
deploy.local.services=false
xacml.enabled=true
database.mysql.jdbcDriverClass=com.mysql.jdbc.Driver
fedora.serverHost=localhost
tomcat.http.port=8080
database=mysql
database.driver=/usr/share/java/mysql.jar
fedora.serverContext=fedora
llstore.type=akubra-fs
tomcat.home=/var/lib/tomcat7
database.mysql.jdbcURL=jdbc\:mysql\://myexample/fedoradb?useUnicode\=true&amp;characterEncoding\=UTF-8&amp;autoReconnect\=true
fedora.home=/usr/local/fedora
install.type=custom
servlet.engine=existingTomcat
fedora.admin.pass=MYPASSWORD

```

You will NOT have to set JAVA_HOME or CATALINA_HOME if you are using Ubuntu's version of tomcat and java. This is because they are set by the package
manager when they are installed. Changing these environmental variables is NOT recommended.


Once installed, you should finish configuring fedora by changing the password for `fedoraIntCallUser` in these files:
```
vim /usr/local/fedora/server/config/fedora-users.xml
vim /usr/local/fedora/server/config/beSecurity.xml
```

Also, change the default PID namespace to something other than 'changeme'. One suggestion is `uncategorized`, as this is a default/catch-all namespace:
```
vim /usr/local/fedora/server/config/fedora.fcfg
```


**Possible Problems**  
Once you attempt to start the fedora instance, if it fails and you run across this error in the logs:
```
The SQL Rebuild attempted on 31 Dec 1969 23:59:59 GMT did not finish successfully, which may compromise the repo. Please re-run the SQL rebuild
```
Then you may have to manually re-build the database. This can be done by running `/usr/local/fedora/server/bin/fedora-rebuild.sh` and following the prompts
to "Rebuild SQL database". If you have problems running this command, see the relevant part below.  

If you initially installed Fedora Commons without ResourceIndex enabled, you may encounter this error later on:
```
Warning: Invalid argument supplied for foreach() in template_preprocess_islandora_basic_collection()
```
To fix this, you have to first enable the ResourceIndex by editing the `fedora.fcfg`, locating the "org.fcrepo.server.resourceIndex.ResourceIndex" and setting
the "level" to "1". For example:
```
vim /usr/local/fedora/server/config/fedora.fcfg
```
And ensure the ResourceIndex section is similar to this snippet:
```
<module role="org.fcrepo.server.resourceIndex.ResourceIndex" class="org.fcrepo.server.resourceIndex.ResourceIndexModule">
  <comment>Supports the ResourceIndex.</comment>
    <param name="level" value="1">
```

Once the setting has been changed, you have to manually rebuild the ResourceIndex. Ensure the Fedora instance is stopped, which can be done by stopping Tomcat.
```
systemctl stop tomcat7
```
And running `/usr/local/fedora/server/bin/fedora-rebuild.sh` and following the prompts to "Rebuild the Resource Index".  

**Problems running fedora-rebuild.sh**  
If you are having problems running `fedora-rebuild.sh` due to either of these errors:
```
ERROR: The FEDORA_HOME environment variable is not defined.
ERROR: The CATALINA_HOME environment variable is not defined.
```
You can manually set them at the beginning of the command line for just this one command. For example, to set both of them and run as the Tomcat user:
```
sudo -Hu tomcat7 FEDORA_HOME=/usr/local/fedora CATALINA_HOME=/var/lib/tomcat7 /usr/local/fedora/server/bin/fedora-rebuild.sh
```

**Test that it works**  
At this point, you should be able to start fedora in the tomcat manager interface, or just restart the tomcat instance. If successfully started, you should be
able to visit `http://myserver:8080/fedora/` in a browser and see a Fedora page running.


Rebuilding Content
------------------------------------
Fedora Commons can rebuild records from scratch given the source files. These are typically found in Fedora's `data/` directory. To have Fedora Commons re-create the
records for Islandora/Solr, ensure the files are copied/synced to the data directory, and then run the `fedora-rebuild.sh` script. Fedora should be stopped while this
happens. Since the rebuild only adds new records and does not delete old ones, there are two different sets of instructions based on if you are doing the rebuild on a fresh
server or if you are rebuilding data on an existing one.  

### Rebuilding on a clean server    

Run the below commands to copy the files to the data directory and then run the Fedora rebuild script.  
```
systemctl stop tomcat7
rsync -r -v /my/backup/of/fedora/data/objectStore /usr/local/fedora/data/objectStore
rsync -r -v /my/backup/of/fedora/data/datastreamStore /usr/local/fedora/data/datastreamStore
chown -R tomcat7:tomcat7 /usr/local/fedora/data
sudo -Hu tomcat7 FEDORA_HOME=/usr/local/fedora CATALINA_HOME=/var/lib/tomcat7 /usr/local/fedora/server/bin/fedora-rebuild.sh
```

You will be prompted as to what to rebuild, first "Rebuild the Resource Index". Once completed, run the `fedora-rebuild.sh` script again, and "Rebuild SQL database". This
process should have re-added all the content you copied over from backups. Be sure to re-start the Tomcat instance.
```
systemctl start tomcat7
```

### Rebuilding on an existing server    

Run the below commands to copy the files to the data directory.  
```
service tomcat7 stop
cp  -r /my/backup/of/fedora/data/objectStore /usr/local/fedora/data/objectStore
cp  -r /my/backup/of/fedora/data/datastreamStore /usr/local/fedora/data/datastreamStore
chown -R tomcat7:tomcat7 /usr/local/fedora/data
```

Take a SQL dump of the Fedora database as a backup in case anything goes wrong during the rebuild.  
```
mysqldump -h [db_server] -p [fedora_commons_db_name] > [dump_filename].sql
```

Login to the database and drop the existing tables in it so they will be recreated from scratch.  
```
drop table dcDates;
drop table doFields;
drop table doRegistry;
drop table fcrepoRebuildStatus;
drop table modelDeploymentMap;
drop table pidGen;
```

Rebuild the Fedora database.  Run the below command and select option 2 for rebuilding the database, then option 1 to confirm.  
```
sudo -Hu tomcat7 FEDORA_HOME=/usr/local/fedora CATALINA_HOME=/var/lib/tomcat7 /usr/local/fedora/server/bin/fedora-rebuild.sh
```

Remove the existing `/usr/local/fedora/data/resourceIndex` directory so that it can be recreated from scratch.  
```
mv /usr/local/fedora/data/resourceIndex /usr/local/fedora/data/resourceIndex_old
```

Rebuild the resource index. Run the below command and select option 1 for rebuilding the resource index, then option 1 to confirm.
```
sudo -Hu tomcat7 FEDORA_HOME=/usr/local/fedora CATALINA_HOME=/var/lib/tomcat7 /usr/local/fedora/server/bin/fedora-rebuild.sh
```

If there were no errors in the rebuild you can remove the old resourceIndex directory and the SQL dump file.  
```
rm -r /usr/local/fedora/data/resourceIndex_old
rm [dump_filename].sql
```

Start the tomcat instance.  
```
service tomcat7 start
```
